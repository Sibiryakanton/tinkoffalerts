import asyncio
import typing as t

from aiogram import Bot
from dependency_injector import containers, providers
from django.conf import settings

from core.integrations import (
    SendEmailController,
    TelegramChatBotDispatcher,
    TinkoffAPIRESTManager,
    TinkoffInvestStreaminWebsocket,
    VKManager,
)

_TOUT = t.TypeVar("_TOUT")
Producer = t.Callable[[], _TOUT]


class ProductionContainer(containers.DeclarativeContainer):

    tinkoff_api_manager: Producer["TinkoffAPIRESTManager"] = providers.Singleton(
        TinkoffAPIRESTManager, token=settings.TINKOFF_INVEST_TOKEN_SANDBOX
    )
    tinkoff_websocket: Producer["TinkoffInvestStreaminWebsocket"] = providers.Singleton(
        TinkoffInvestStreaminWebsocket, token=settings.TINKOFF_INVEST_TOKEN_SANDBOX
    )
    aiogram_bot: Producer["Bot"] = providers.Singleton(
        Bot, token=settings.TELEGRAM_BOT_KEY
    )

    aiogram_dispatcher: Producer["TelegramChatBotDispatcher"] = providers.Singleton(
        TelegramChatBotDispatcher, bot=aiogram_bot, loop=asyncio.get_event_loop()
    )

    email_client: Producer["SendEmailController"] = providers.Singleton(
        SendEmailController,
        server=settings.EMAIL_HOST,
        port=settings.EMAIL_PORT,
        email=settings.EMAIL_HOST_USER,
        password=settings.EMAIL_HOST_PASSWORD,
    )

    vk_api_manager: Producer["VKManager"] = providers.Singleton(
        VKManager, access_token=settings.VK_SERVICE_VK_GROUP_KEY
    )
