from .base import INSTALLED_APPS, MIDDLEWARE, get_env_value

IS_SILK = get_env_value("TINKOFF_ALERTS_SILK")

if IS_SILK:
    MIDDLEWARE.append("silk.middleware.SilkyMiddleware")
    INSTALLED_APPS.append("silk")

    SILKY_PYTHON_PROFILER = True
