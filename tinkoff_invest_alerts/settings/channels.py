from .base import env

BROKER_SERVER = env("TINKOFF_ALERTS_CELERY_DOMAIN")
BROKER_PORT = 6379

splitted = env("TINKOFF_ALERTS_CELERY_DOMAIN").split(":")
if len(splitted) > 1:
    BROKER_SERVER, BROKER_PORT = splitted
else:
    BROKER_SERVER, BROKER_PORT = splitted[0], 6379

CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {
            "hosts": [(BROKER_SERVER, BROKER_PORT)],
        },
    },
}
