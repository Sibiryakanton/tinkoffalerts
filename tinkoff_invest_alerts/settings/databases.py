from .base import get_env_value

# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": get_env_value("TINKOFF_ALERTS_DB", default="tinkoff_alerts_db"),
        "USER": get_env_value("TINKOFF_ALERTS_DBUSER", default="postgres"),
        "PASSWORD": get_env_value("TINKOFF_ALERTS_DBPASSWORD", default="postgrespass"),
        "HOST": get_env_value("TINKOFF_ALERTS_DBHOST", default="localhost"),
        "PORT": get_env_value("TINKOFF_ALERTS_DBPORT", default=5432),
    }
}
