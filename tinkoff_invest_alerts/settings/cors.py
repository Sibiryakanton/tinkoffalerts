import typing as t

CORS_ALLOWED_ORIGINS: t.List[str] = []
CORS_ORIGIN_ALLOW_ALL = True
