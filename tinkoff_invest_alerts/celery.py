import os

from celery import Celery
from django.conf import settings

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "tinkoff_invest_alerts.settings")

app = Celery("tinkoff_invest_alerts")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()
app.conf.broker_url = settings.CELERY_BROKER_URL
app.conf.beat_schedule = {
    "clear_excess_candles": {
        "task": "core.tasks.clear_excess_candles",
        "schedule": 3600,
    },
}
