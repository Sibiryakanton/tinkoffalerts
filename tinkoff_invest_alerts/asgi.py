"""
ASGI config for tinkoff_invest_alerts project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/asgi/
"""

import os

from django.core.asgi import get_asgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "tinkoff_invest_alerts.settings")

asgi_app = get_asgi_application()

from channels.routing import ProtocolTypeRouter  # noqa: E402

from websocket_api.urls import router  # noqa: E402
from websocket_api.websockets_auth import CustomTokenAuthMiddleware  # noqa: E402

application = ProtocolTypeRouter(
    {
        "http": asgi_app,
        "websocket": CustomTokenAuthMiddleware(router),
    }
)
