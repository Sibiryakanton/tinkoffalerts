import typing as t

from django.conf import settings


def current_version(request: t.Any) -> t.Dict[str, t.Any]:
    return {
        "CURRENT_VERSION": settings.CURRENT_VERSION,
    }
