"""tinkoff_invest_alerts URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import typing as t

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from django.views.generic.base import RedirectView


def trigger_error(request: t.Any) -> None:
    division_by_zero = 1 / 0
    print(division_by_zero)


urlpatterns = [
    path("admin/", admin.site.urls),
    path("rest/", include(("rest.urls", "rest"), namespace="rest")),
    path("docs/", include("rest.urls.schemes")),
    path("error-debug/", trigger_error),
    path("", RedirectView.as_view(url="/admin")),
]
if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.IS_SILK:
    urlpatterns += [path("silk/", include("silk.urls", namespace="silk"))]
