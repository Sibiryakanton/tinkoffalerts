import traceback
import typing as t
import uuid

from django.conf import settings
from django.db import models
from django.db.models.signals import post_save
from django.db.transaction import atomic
from django.dispatch import receiver
from django.utils.timezone import now, timedelta
from django.utils.translation import gettext_lazy as _
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from accounts.tasks import send_reset_password_code
from configuration.di import ProductionContainer

if t.TYPE_CHECKING:
    from .user import User


class PasswordChangeOrderManager(models.Manager):
    def get_for_activating(self) -> models.QuerySet["PasswordChangeOrder"]:
        return super().get_queryset().filter(activated=False, will_expire_at__gte=now())


class PasswordChangeOrder(models.Model):
    """
    Password change request model.
     In fact, this is a one-time application for obtaining
     authorization token by code, and the password will be changed by an already authorized user.
    """

    uuid = models.UUIDField(_("Activation Code"), default=uuid.uuid4, unique=True)
    user: "User" = models.ForeignKey(
        "User",
        on_delete=models.CASCADE,
        related_name="password_orders",
        verbose_name=_("User"),
    )

    sent_at = models.DateTimeField(_("Order sending datetime"), null=True, blank=True)
    activated = models.BooleanField(default=False, verbose_name=_("Is code activated"))
    will_expire_at = models.DateTimeField(
        _("The datetime of deactivating code"), null=True
    )
    error_response = models.TextField(
        verbose_name=_("Error description"), null=True, blank=True
    )

    objects = PasswordChangeOrderManager()

    def save(self, *args: t.Any, **kwargs: t.Any) -> None:
        if not self.will_expire_at:
            self.will_expire_at = now() + timedelta(minutes=5)
        super().save(*args, **kwargs)

    def send(self) -> None:
        """Send email with activation url."""
        try:
            self.send_email()
            self.sent_at = now()
            self.save()
        except Exception:
            self.error_response = traceback.format_exc()
            self.save()

    def send_email(self) -> None:
        title = "TinkoffAlerts - activation code"
        activation_frontend_url = "{}activation/{}".format(
            settings.FRONTEND_URL, self.uuid
        )
        text = "Your url for password change - {}".format(activation_frontend_url)
        email_controller = ProductionContainer.email_client()
        email_controller.send_email(
            receiver=self.user.email, subject=title, message=text
        )

    @atomic
    def activate(self) -> t.Dict[str, str]:
        self.activated = True
        self.save()
        payload = JSONWebTokenAuthentication.jwt_create_payload(self.user)
        token = JSONWebTokenAuthentication.jwt_encode_payload(payload)
        data = {"token": token}
        return data

    class Meta:
        verbose_name = _("password change order")
        verbose_name_plural = _("password change orders")


@receiver(post_save, sender=PasswordChangeOrder)
def send_reset_password_code_signal(
    sender: t.Any, instance: PasswordChangeOrder, **kwargs: t.Any
) -> None:
    PasswordChangeOrder.objects.filter(user=instance.user.id).exclude(
        pk=instance.pk
    ).delete()
    if not instance.sent_at:
        send_reset_password_code.delay(instance.id)
