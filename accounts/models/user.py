import typing as t

from django.contrib.auth.models import AbstractUser, UserManager
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext_lazy as _

from accounts.messages import TELEGRAM_ID_TAKEN


class CustomUserManager(UserManager):
    def get_by_telegram_id(self, telegram_id: str) -> t.Optional["User"]:
        return super().get_queryset().filter(telegram_id=telegram_id).first()  # type: ignore


class User(AbstractUser):
    vk_id = models.CharField(_("VK profile ID"), max_length=20, null=True, blank=True)
    telegram_id = models.CharField(
        _("Telegram profile ID"), max_length=20, null=True, blank=True
    )
    is_vk_enabled = models.BooleanField(default=True)
    is_telegram_enabled = models.BooleanField(default=True)
    binding_id = models.CharField(
        null=True,
        blank=True,
        max_length=50,
        verbose_name=_("The secret string for merging with other profiles"),
        help_text=_("the other profile will be deleted during merge"),
    )
    objects = CustomUserManager()

    def clean(self) -> None:
        if self.telegram_id:
            is_telegram_id_taken = (
                User.objects.filter(telegram_id=self.telegram_id)
                .exclude(pk=self.pk)
                .exists()
            )
            if is_telegram_id_taken:
                ValidationError({"telegram_id": TELEGRAM_ID_TAKEN})
