import typing as t

from django.db import models, transaction
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _
from model_utils.models import TimeStampedModel

from accounts.tasks import send_vk_notification

if t.TYPE_CHECKING:
    from accounts.models import User


class VKNotification(TimeStampedModel):
    """Model of notification in VK Profile after triggering market signal."""

    user: "User" = models.ForeignKey(
        "User", on_delete=models.CASCADE, verbose_name=_("User")
    )
    message = models.TextField(_("Text of message"))
    sent = models.BooleanField(_("Sent"), default=False)
    error = models.TextField(_("Error response"), null=True, blank=True)

    class Meta:
        verbose_name = _("VK notification")
        verbose_name_plural = _("VK notifications")


@receiver(post_save, sender=VKNotification)
def send_reset_password_code_signal(
    sender: VKNotification, instance: VKNotification, **kwargs: t.Any
) -> None:
    user: "User" = instance.user
    is_need_send_msg = (
        user.vk_id
        and user.is_vk_enabled
        and not instance.sent
        and instance.error in [None, ""]
    )
    if is_need_send_msg:
        transaction.on_commit(lambda: send_vk_notification.delay(instance.id))
