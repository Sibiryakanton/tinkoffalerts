from django.utils.translation import gettext_lazy as _

TELEGRAM_ID_TAKEN = _("Telegram ID already taken by other user.")
