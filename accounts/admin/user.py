import typing as t

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.db.models import QuerySet
from django.utils.translation import gettext_lazy as _

from accounts.models import User


@admin.register(User)
class CustomUserAdmin(UserAdmin):
    fieldsets = (
        (None, {"fields": ("username", "password")}),
        (
            _("Personal info"),
            {
                "fields": (
                    "first_name",
                    "last_name",
                    "email",
                    "vk_id",
                    "telegram_id",
                    "is_vk_enabled",
                    "is_telegram_enabled",
                )
            },
        ),
        (
            _("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
        (_("Important dates"), {"fields": ("last_login", "date_joined")}),
    )

    def get_queryset(self, request: t.Any) -> QuerySet[User]:
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(id=request.user.id)
        return queryset

    def get_fieldsets(self, request: t.Any, obj: t.Optional[User] = None) -> t.Any:
        if not request.user.is_superuser:
            return (
                (None, {"fields": ("username", "password")}),
                (
                    _("Personal info"),
                    {"fields": ("first_name", "last_name", "email", "vk_id")},
                ),
            )
        return super().get_fieldsets(request, obj)
