import typing as t

from django.forms import ModelForm

from accounts.models import User


class UserFieldStrictForm(ModelForm):
    """
    We allow users operate their data with using base Django admin panel.
    One of the nuances that must be respected in this case is the complete
    isolation of some users from others.
    That is, for example, one user should not have access to information
     about other users, see their signals, and even
      more so create signals pointing to other users.
    """

    def __init__(self, *args: t.Any, **kwargs: t.Any) -> None:
        super().__init__(*args, **kwargs)
        if not self.current_user.is_superuser:
            self.fields["user"].queryset = User.objects.filter(id=self.current_user.id)
            self.fields["user"].initial = self.current_user.id
