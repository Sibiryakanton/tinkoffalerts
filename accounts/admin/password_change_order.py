from django.contrib import admin

from accounts.models import PasswordChangeOrder


@admin.register(PasswordChangeOrder)
class PasswordChangeOrderAdmin(admin.ModelAdmin):
    list_display = ["id", "uuid", "user", "sent_at", "activated"]
    list_display_links = [
        "id",
        "user",
        "sent_at",
    ]
    search_fields = ["user__id", "user__username"]
