import typing as t

from django.contrib import admin
from django.db.models import QuerySet

from accounts.models import VKNotification

from .user_field_strict import UserFieldStrictForm


@admin.register(VKNotification)
class VKNotificationAdmin(admin.ModelAdmin):
    form = UserFieldStrictForm
    list_display = ["id", "user", "sent", "created", "message"]
    list_display_links = ["id", "user", "sent", "created"]
    search_fields = ["user__id", "user__username"]
    list_filter = ["sent"]

    def get_form(
        self, request: t.Any, obj: t.Optional[VKNotification] = None, **kwargs: t.Any
    ) -> "VKNotificationAdmin":
        form: VKNotificationAdmin = super().get_form(request, obj, **kwargs)
        form.current_user = request.user
        return form

    def get_queryset(self, request: t.Any) -> QuerySet[VKNotification]:
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(user=request.user)
        return queryset
