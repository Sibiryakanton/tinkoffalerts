import typing as t
from unittest.mock import patch

from django.conf import settings

from accounts.admin import CustomUserAdmin, VKNotificationAdmin
from accounts.models import PasswordChangeOrder, User, VKNotification
from accounts.tasks import send_reset_password_code, send_vk_notification
from configuration.di import ProductionContainer
from core.base_test import BaseDjangoTestCase, MockHTTPResponse


class MockSMTP_SSL:
    """
    Mock object for smtplib.SMTP_SSL
    """

    def __init__(self) -> None:
        super().__init__()

    def __enter__(self, *args: t.Any, **kwargs: t.Any) -> "MockSMTP_SSL":
        return self

    def __exit__(self, exc_type: t.Any, exc_val: t.Any, exc_tb: t.Any) -> None:
        pass

    def login(self, email: str, passsword: str) -> None:
        pass


class MockSMTPSSLSuccess(MockSMTP_SSL):
    def sendmail(self, *args: t.Any, **kwargs: t.Any) -> int:
        return 1


class MockSMTPSSLError(MockSMTP_SSL):
    def sendmail(self, *args: t.Any, **kwargs: t.Any) -> None:
        raise KeyError("Exception")


class MockSMTPSSLFail(MockSMTP_SSL):
    def sendmail(self, *args: t.Any, **kwargs: t.Any) -> None:
        pass


class AccountsTestCase(BaseDjangoTestCase):
    def test_vk_notifications_admin_get_form(self) -> None:
        notification = VKNotification.objects.create(
            user=self.admin_user, message="Some message"
        )
        model_admin = VKNotificationAdmin(model=VKNotification, admin_site=self.site)
        model_admin.get_form(
            request=self.request_mock(self.admin_user), obj=notification
        )

    def test_vk_notifications_admin_get_queryset_admin(self) -> None:
        VKNotification.objects.create(user=self.admin_user, message="Some message")

        model_admin = VKNotificationAdmin(model=VKNotification, admin_site=self.site)
        request = self.request_mock(self.admin_user)
        notification_retrieved = model_admin.get_queryset(request=request).exists()
        self.assertTrue(notification_retrieved)

    def test_vk_notifications_admin_get_queryset_admin_quest_notifications(
        self,
    ) -> None:
        VKNotification.objects.create(user=self.quest, message="Some message")
        model_admin = VKNotificationAdmin(model=VKNotification, admin_site=self.site)
        request = self.request_mock(self.admin_user)
        notification_retrieved = model_admin.get_queryset(request=request).exists()
        self.assertTrue(notification_retrieved)

    def test_vk_notifications_admin_get_queryset_quest(self) -> None:
        VKNotification.objects.create(user=self.admin_user, message="Some message")

        model_admin = VKNotificationAdmin(model=VKNotification, admin_site=self.site)
        request = self.request_mock(self.quest)
        notification_retrieved = model_admin.get_queryset(request=request).exists()
        self.assertFalse(notification_retrieved)

    def test_user_admin_get_queryset_admin(self) -> None:
        model_admin = CustomUserAdmin(model=User, admin_site=self.site)
        request = self.request_mock(self.admin_user)
        queryset = model_admin.get_queryset(request=request)
        is_all_users = queryset.count() > 1
        self.assertTrue(is_all_users)

    def test_user_admin_get_queryset_quest(self) -> None:
        model_admin = CustomUserAdmin(model=User, admin_site=self.site)
        request = self.request_mock(self.quest)
        is_all_users = model_admin.get_queryset(request=request).count() > 1
        self.assertFalse(is_all_users)

    def test_user_admin_get_fieldsets_admin(self) -> None:
        model_admin = CustomUserAdmin(model=User, admin_site=self.site)
        request = self.request_mock(self.admin_user)
        model_admin.get_fieldsets(request=request, obj=self.quest)

    def test_user_admin_get_fieldsets_quest(self) -> None:
        model_admin = CustomUserAdmin(model=User, admin_site=self.site)
        request = self.request_mock(self.quest)
        model_admin.get_fieldsets(request=request, obj=self.quest)

    @patch("requests.post")
    def test_send_vk_notifications(self, request_post_mock: t.Any) -> None:
        response = MockHTTPResponse(status_code=200, json={"status": "OK"})
        request_post_mock.return_value = response
        notification = VKNotification.objects.create(
            user=self.admin_user, message="Some message"
        )
        send_vk_notification(notification.id)
        notification.refresh_from_db()
        self.assertTrue(notification.sent)

    @patch("requests.post")
    def test_send_vk_notifications_fail(self, request_post_mock: t.Any) -> None:
        response = MockHTTPResponse(status_code=200, json={"error": "some_error"})
        request_post_mock.return_value = response

        notification = VKNotification.objects.create(
            user=self.admin_user, message="Some message"
        )
        send_vk_notification(notification.id)
        notification.refresh_from_db()
        self.assertFalse(notification.sent)
        self.assertIsNotNone(notification.error)

    @patch("smtplib.SMTP_SSL")
    def test_send_password_codes(self, smtp_mock: t.Any) -> None:
        smtp_mock.return_value.__enter__.return_value = MockSMTPSSLSuccess()

        order = PasswordChangeOrder.objects.create(user=self.admin_user)
        send_reset_password_code(order.id)
        order.refresh_from_db()

        self.assertIsNotNone(order.sent_at)

    @patch("smtplib.SMTP_SSL")
    def test_send_password_codes_err(self, smtp_mock: t.Any) -> None:
        smtp_mock.return_value.__enter__.return_value = MockSMTPSSLError()

        settings.EMAIL_HOST_PASSWORD = "some_str"
        order = PasswordChangeOrder.objects.create(user=self.admin_user)
        send_reset_password_code(order.id)
        order.refresh_from_db()
        self.assertIsNone(order.sent_at)
        self.assertIsNotNone(order.error_response)

    @patch("smtplib.SMTP_SSL")
    def test_send_email_err(self, smtp_mock: t.Any) -> None:
        smtp_mock.return_value.__enter__.return_value = MockSMTPSSLFail()
        email = "admin@mail.ru"
        title = "TinkoffAlerts"
        text = "Message"
        email_client = ProductionContainer.email_client()

        email_client.send_email(receiver=email, subject=title, message=text)
