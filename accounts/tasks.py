from celery import shared_task

from configuration.di import ProductionContainer


@shared_task
def send_vk_notification(notification_id: int) -> None:
    from accounts.models import VKNotification

    vk_manager = ProductionContainer.vk_api_manager()
    notification = VKNotification.objects.get(id=notification_id)
    response = vk_manager.send_message(
        user_id=notification.user.vk_id, message=notification.message
    )
    if "error" in response:
        notification.error = response
    else:
        notification.sent = True
    notification.save()


@shared_task
def send_reset_password_code(code_id: int) -> None:
    from accounts.models import PasswordChangeOrder

    code = PasswordChangeOrder.objects.get(id=code_id)
    code.send()
