Backend for project "Tinkoff Investment Alerts".


Main function of project: creating signals for monitoring charts of products 
on "Tinkoff Investments" platform.

The default signals in Tinkoff terminal allows creating signals like
"if [COMPANY NAME] price will rise to 725, alert me".

This project allows creating signals like
"if [COMPANY NAME] price will rise on X percents in Y minutes, alert me".

When signal triggering, user will get notification (only in VK profile yet). 
The user should add VK profile id in profile info for subscription.

Users should be created manually by the administrator with access 
to the panel (the administrator must create the necessary 
user groups to manage permissions himself) 


How to install locally:
git clone https://gitlab.com/Sibiryakanton/tinkoffalerts.git
create .env file (use .env.example as template)

docker-compose -f docker-compose.yaml --profile deploy up --build -d

for tests:
docker-compose run test
__
Dev shortcuts:

flake8 . --exclude=*/migrations/*,*settings*,__init__.py,apps.py --max-line-length=120
