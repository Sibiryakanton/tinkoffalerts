#!/bin/bash

cd /app
python manage.py migrate
python manage.py collectstatic --noinput
exec gunicorn tinkoff_invest_alerts.wsgi --bind=:8000
