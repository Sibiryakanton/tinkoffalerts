#!/bin/bash

python manage.py migrate
python manage.py collectstatic --noinput

exec daphne tinkoff_invest_alerts.asgi:application -b 0.0.0.0 -p 8000
