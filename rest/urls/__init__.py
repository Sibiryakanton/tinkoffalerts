from django.urls import path
from django.utils.decorators import method_decorator
from drf_yasg.utils import swagger_auto_schema
from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import ObtainJSONWebTokenView

from rest.views import (
    MarketProductViewSet,
    MarketSignalViewSet,
    PasswordChangeOrderViewSet,
    UserViewSet,
)

router = DefaultRouter()
router.register("users", UserViewSet)
router.register("password_change_orders", PasswordChangeOrderViewSet)
router.register("market_products", MarketProductViewSet)
router.register("market_signals", MarketSignalViewSet)


@method_decorator(
    swagger_auto_schema(
        operation_id="Get auth token by username and password",
        operation_description="",
        tags=["accounts"],
    ),
    "post",
)
class CustomObtainJSONWebTokenView(ObtainJSONWebTokenView):
    pass


urlpatterns = [
    path("get_token/", CustomObtainJSONWebTokenView.as_view(), name="get_token"),
] + router.urls
