import typing as t

from django.db.models import QuerySet
from django_filters import rest_framework as filters

from core.models import MarketProduct, MarketSignal


class MarketProductFilter(filters.FilterSet):
    search = filters.CharFilter(method="search_filter")

    def search_filter(
        self, queryset: QuerySet["MarketSignal"], name: str, value: t.Any
    ) -> QuerySet[MarketProduct]:
        if value:
            queryset = queryset.filter(title__icontains=value)
        return queryset

    class Meta:
        model = MarketProduct
        fields = ["search", "title", "product_type"]


class MarketSignalFilter(filters.FilterSet):
    product_name = filters.CharFilter(method="search_product_name")

    def search_product_name(
        self, queryset: QuerySet["MarketSignal"], name: str, value: t.Any
    ) -> QuerySet["MarketSignal"]:
        if value:
            queryset = queryset.filter(product__title__icontains=value)
        return queryset

    class Meta:
        model = MarketSignal
        fields = ["product_name", "is_active", "price_percent_change", "time_interval"]
