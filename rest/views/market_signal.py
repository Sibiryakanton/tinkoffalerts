from django.db.models import QuerySet
from django.utils.decorators import method_decorator
from drf_yasg.utils import swagger_auto_schema
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet

from core.models import MarketSignal
from rest.filters import MarketSignalFilter
from rest.serializers import MarketSignalSerializer

tags = ["signals"]


@method_decorator(
    swagger_auto_schema(
        operation_id="List of signals",
        tags=tags,
        operation_description="",
        responses={},
    ),
    "list",
)
@method_decorator(
    swagger_auto_schema(
        operation_id="Retrieve the product",
        tags=tags,
        operation_description="",
        responses={},
    ),
    "retrieve",
)
@method_decorator(
    swagger_auto_schema(
        operation_id="Create signal", tags=tags, operation_description="", responses={}
    ),
    "create",
)
@method_decorator(
    swagger_auto_schema(
        operation_id="Update signal",
        tags=tags,
        deprecated=True,
        operation_description="",
        responses={},
    ),
    "update",
)
@method_decorator(
    swagger_auto_schema(
        operation_id="Partial update signal",
        tags=tags,
        operation_description="",
        responses={},
    ),
    "partial_update",
)
@method_decorator(
    swagger_auto_schema(
        operation_id="Delete signal", tags=tags, operation_description="", responses={}
    ),
    "destroy",
)
class MarketSignalViewSet(ModelViewSet):
    queryset = MarketSignal.objects.all()
    permission_classes = [IsAuthenticated]
    serializer_class = MarketSignalSerializer
    filterset_class = MarketSignalFilter

    def get_queryset(self) -> QuerySet["MarketSignal"]:
        self.queryset = self.queryset.filter(user=self.request.user)
        return super().get_queryset()
