import typing as t

from django.utils.decorators import method_decorator
from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import action
from rest_framework.mixins import CreateModelMixin
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED
from rest_framework.viewsets import GenericViewSet
from rest_framework_jwt.serializers import VerifyAuthTokenSerializer

from accounts.models import PasswordChangeOrder
from rest.serializers import PasswordChangeOrderSerializer


@method_decorator(
    swagger_auto_schema(
        operation_id="Create password change order",
        tags=["accounts"],
        operation_description="",
        responses={},
    ),
    "create",
)
class PasswordChangeOrderViewSet(GenericViewSet, CreateModelMixin):
    queryset = PasswordChangeOrder.objects.get_for_activating()
    permission_classes = [AllowAny]
    lookup_field = "uuid"

    def get_serializer_class(self) -> t.Any:
        if self.action == "create":
            self.serializer_class = PasswordChangeOrderSerializer
        return super().get_serializer_class()

    def create(self, request: t.Any, *args: t.Any, **kwargs: t.Any) -> Response:
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.create_order()
        return Response(status=HTTP_201_CREATED)

    @swagger_auto_schema(
        operation_id="Get auth token by change order code",
        tags=["accounts"],
        request_body=None,
        operation_description="",
        responses={200: VerifyAuthTokenSerializer},
    )
    @action(detail=True, methods=["POST"])
    def activate(self, request: t.Any, *args: t.Any, **kwargs: t.Any) -> Response:
        order = self.get_object()
        data = order.activate()
        return Response(data)
