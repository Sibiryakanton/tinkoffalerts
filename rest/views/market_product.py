from django.utils.decorators import method_decorator
from drf_yasg.utils import swagger_auto_schema
from rest_framework.permissions import AllowAny
from rest_framework.viewsets import ReadOnlyModelViewSet

from core.models import MarketProduct
from rest.filters import MarketProductFilter
from rest.serializers import MarketProductSerializer


@method_decorator(
    swagger_auto_schema(
        operation_id="List of products",
        tags=["products"],
        operation_description="",
        responses={},
    ),
    "list",
)
@method_decorator(
    swagger_auto_schema(
        operation_id="Retrieve the product",
        tags=["products"],
        operation_description="",
        responses={},
    ),
    "retrieve",
)
class MarketProductViewSet(ReadOnlyModelViewSet):
    queryset = MarketProduct.objects.all()
    permission_classes = [AllowAny]
    serializer_class = MarketProductSerializer
    filterset_class = MarketProductFilter
    lookup_field = "figi"
