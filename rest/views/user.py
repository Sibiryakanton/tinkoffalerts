import typing as t

from django.utils.decorators import method_decorator
from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import action
from rest_framework.mixins import CreateModelMixin
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from accounts.models import User
from rest.serializers import PasswordSerializer, SignUpSerializer, UserSerializer


@method_decorator(
    swagger_auto_schema(
        operation_id="Sign Up",
        operation_description="",
        tags=["accounts"],
    ),
    "create",
)
class UserViewSet(GenericViewSet, CreateModelMixin):
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def get_serializer_class(self) -> t.Any:
        if self.action == "create":
            self.serializer_class = SignUpSerializer
        elif self.action == "change_password":
            self.serializer_class = PasswordSerializer
        return super().get_serializer_class()

    def get_permissions(self) -> t.Any:
        if self.action == "create":
            self.permission_classes = [AllowAny]
        return super().get_permissions()

    @swagger_auto_schema(
        operation_id="Profile Info",
        tags=["accounts"],
        operation_description="",
        responses={200: UserSerializer},
    )
    @action(detail=False, methods=["GET"])
    def me(self, request: t.Any, *args: t.Any, **kwargs: t.Any) -> Response:
        response = self.get_serializer(request.user)
        return Response(response.data)

    @swagger_auto_schema(
        operation_id="Update user profile info",
        tags=["accounts"],
        operation_description="",
        responses={200: UserSerializer},
    )
    @action(detail=False, methods=["PATCH"])
    def edit(self, request: t.Any, *args: t.Any, **kwargs: t.Any) -> Response:
        partial = kwargs.pop("partial", True)
        user = request.user
        serializer = self.get_serializer(
            user,
            data=request.data,
            partial=partial,
            context=self.get_serializer_context(),
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    @swagger_auto_schema(
        operation_id="Change password", tags=["accounts"], responses={204: None}
    )
    @action(detail=False, methods=["POST"])
    def change_password(
        self, request: t.Any, *args: t.Any, **kwargs: t.Any
    ) -> Response:
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.change_pass()
        return Response(serializer.data)
