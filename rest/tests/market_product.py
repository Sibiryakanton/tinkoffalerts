from .base import BaseTestCase


class MarketProductTestCase(BaseTestCase):
    def test_list_search(self) -> None:
        url = self.get_rel_url("marketproduct-list")
        search = {"search": "Tesla"}
        response = self.client.get(url, data=search)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()["count"], 1)
