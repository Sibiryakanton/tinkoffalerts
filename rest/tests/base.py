import typing as t

from django.shortcuts import reverse
from rest_framework.test import APITestCase
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from accounts.models import User
from core.base_test import BaseDjangoTestCase


class BaseTestCase(BaseDjangoTestCase, APITestCase):
    root_rel = "rest"

    def get_rel_url(self, rel_name: str, *args: t.Any, **kwargs: t.Any) -> str:
        """Get relative link for request by link name."""
        url_name = "{}:{}".format(self.root_rel, rel_name)
        reversed_url: str = reverse(url_name, args=args, kwargs=kwargs)
        return reversed_url

    def get_token_no_request(self, user: t.Optional[User] = None) -> str:
        """Get token without api request."""
        payload = JSONWebTokenAuthentication.jwt_create_payload(user or self.client)
        token: str = JSONWebTokenAuthentication.jwt_encode_payload(payload)
        return token

    def auth_user(self, user: "User") -> None:
        """Authenticate the user in the test API client."""
        token = "Bearer {}".format(self.get_token_no_request(user))
        self.client.credentials(HTTP_AUTHORIZATION=token)
