from uuid import UUID

from .base import BaseTestCase


class PasswordChangeOrderTestCase(BaseTestCase):
    def test_list(self) -> UUID:
        url = self.get_rel_url("passwordchangeorder-list")
        data = {"email": self.admin_user.email}
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 201)
        order_uuid: UUID = self.admin_user.password_orders.first().uuid
        return order_uuid

    def test_list_no_email(self) -> None:
        url = self.get_rel_url("passwordchangeorder-list")
        data = {"email": "some@mail.com"}
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 400)

    def test_list_search(self) -> None:
        order_uuid = self.test_list()
        url = self.get_rel_url("passwordchangeorder-activate", order_uuid)
        response = self.client.post(url)
        self.assertEqual(response.status_code, 200)
