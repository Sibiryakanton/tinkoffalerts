import typing as t

from accounts.models import User

from .base import BaseTestCase


class UsersTestCase(BaseTestCase):
    def _auth_registered_user(self) -> User:
        register_info = self.test_register_client()
        registered_user: User = User.objects.get(id=register_info["id"])
        self.auth_user(registered_user)
        return registered_user

    def test_register_client(self) -> t.Dict[str, t.Any]:
        url = self.get_rel_url("user-list")
        data = {
            "username": "new_user",
            "first_name": "Тестовый",
            "password_2": self.first_password,
            "password": self.first_password,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 201)
        as_json: t.Dict[str, t.Any] = response.json()
        return as_json

    def test_register_client_wrong_pass(self) -> None:
        url = self.get_rel_url("user-list")
        data = {
            "username": "new_user",
            "first_name": "Тестовый",
            "password_2": self.first_password,
            "password": "other string",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 400)

    def test_get_token(self) -> None:
        register_info = self.test_register_client()
        registered_user = User.objects.get(id=register_info["id"])

        url = self.get_rel_url("get_token")
        data = {"username": registered_user.username, "password": self.first_password}
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 201)

    def test_get_token_wrong_username(self) -> None:
        url = self.get_rel_url("get_token")
        data = {"username": "non_exist_email@mail.ru", "password": self.first_password}
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 400)

    def test_get_token_wrong_pass(self) -> None:
        register_info = self.test_register_client()
        registered_user = User.objects.get(id=register_info["id"])

        url = self.get_rel_url("get_token")
        data = {"username": registered_user.username, "password": "wrong_pass"}
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 400)

    def test_me(self) -> None:
        self._auth_registered_user()
        url = self.get_rel_url("user-me")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_edit(self) -> None:
        registered_user = self._auth_registered_user()

        new_data = {
            "first_name": "New",
        }
        url = self.get_rel_url("user-edit")
        response = self.client.patch(url, data=new_data)
        self.assertEqual(response.status_code, 200)
        registered_user.refresh_from_db()

        self.assertEqual(registered_user.first_name, new_data["first_name"])

    def test_change_password(self) -> None:
        self._auth_registered_user()

        new_data = {
            "password": "new_password123!",
            "password_2": "new_password123!",
        }
        url = self.get_rel_url("user-change-password")
        response = self.client.post(url, data=new_data)
        self.assertEqual(response.status_code, 200)
