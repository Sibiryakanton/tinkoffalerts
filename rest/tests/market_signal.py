from core.enums import SignalPriceTimeIntervalIntegers

from .base import BaseTestCase


class MarketSignalTestCase(BaseTestCase):
    def test_create(self) -> int:
        self.auth_user(self.admin_user)
        url = self.get_rel_url("marketsignal-list")
        data = {
            "price_percent_change": 3,
            "time_interval": SignalPriceTimeIntervalIntegers.min_5.name,
            "product": self.subscribed_product.id,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 201)
        signal_id: int = response.json()["id"]
        return signal_id

    def test_retrieve_other_user(self) -> None:
        signal_id = self.test_create()
        self.auth_user(self.quest)
        url = self.get_rel_url("marketsignal-detail", signal_id)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)
