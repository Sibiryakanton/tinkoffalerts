import typing as t

from rest_framework import serializers
from rest_framework.utils.serializer_helpers import ReturnDict

from accounts.models import User
from rest.messages import USER_PASSWORDS_NO_MATCH

from .user import UserSerializer


class PasswordSerializer(serializers.ModelSerializer):
    password_2 = serializers.CharField()

    def validate(self, attrs: t.Dict[str, t.Any]) -> t.Dict[str, t.Any]:
        if attrs.pop("password_2") != attrs["password"]:
            raise serializers.ValidationError({"password": USER_PASSWORDS_NO_MATCH})
        return attrs

    def change_pass(self) -> None:
        user = self.context["request"].user
        user.set_password(self.validated_data["password"])
        user.save()
        self.instance = user

    def to_representation(self, instance: "User") -> ReturnDict:
        return UserSerializer(instance, context=self.context).data

    class Meta:
        model = User
        fields = ["password", "password_2"]
