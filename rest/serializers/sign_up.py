import typing as t

from rest_framework.utils.serializer_helpers import ReturnDict

from accounts.models import User

from .password_field import PasswordSerializer
from .user import UserSerializer


class SignUpSerializer(PasswordSerializer):
    def validate(self, attrs: t.Dict[str, t.Any]) -> t.Dict[str, t.Any]:
        attrs = super().validate(attrs)
        user = User(**attrs)
        user.clean()
        return attrs

    def create(self, validated_data: t.Dict[str, t.Any]) -> User:
        user: User = super().create(validated_data)
        user.set_password(validated_data["password"])
        user.save()
        return user

    def to_representation(self, instance: User) -> ReturnDict:
        return UserSerializer(instance).data

    class Meta:
        model = User
        fields = ["first_name", "last_name", "username", "password", "password_2"]
