import typing as t

from rest_framework import serializers

from accounts.models import PasswordChangeOrder, User


class PasswordChangeOrderSerializer(serializers.Serializer):
    """Serializer for creating password change order."""

    email = serializers.SlugRelatedField(
        slug_field="email", queryset=User.objects.filter(is_active=True)
    )

    def validate(self, attrs: t.Dict[str, t.Any]) -> t.Dict[str, t.Any]:
        user = attrs["email"]
        order = PasswordChangeOrder(user=user)
        order.clean()
        return attrs

    def create_order(self) -> "PasswordChangeOrder":
        password_change_order: PasswordChangeOrder = PasswordChangeOrder.objects.create(
            user=self.validated_data["email"]
        )
        return password_change_order

    class Meta:
        fields = "__all__"
