import typing as t

from rest_framework import serializers

from core.models import MarketSignal


class MarketSignalSerializer(serializers.ModelSerializer):
    def create(self, validated_data: t.Dict[str, t.Any]) -> t.Any:
        validated_data["user"] = self.context["request"].user
        return super().create(validated_data)

    class Meta:
        model = MarketSignal
        read_only_fields = ["last_triggered_at"]
        exclude = ["user"]
