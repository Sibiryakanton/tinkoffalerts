from rest_framework import serializers

from core.models import MarketProduct


class MarketProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = MarketProduct
        fields = "__all__"
