from django.utils.translation import gettext_lazy as _

USER_PASSWORDS_NO_MATCH = _("The passwords don't match.")
