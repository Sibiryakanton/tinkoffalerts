from django.utils.translation import gettext_lazy as _

from .base_enum import BaseEnum


class SignalPriceTimeInterval(BaseEnum):
    min_5 = _("5 minutes")
    min_30 = _("30 minutes")
    min_1 = _("1 minute")


class SignalPriceTimeIntervalIntegers(BaseEnum):
    min_5 = 5
    min_30 = 30
    min_1 = 1
