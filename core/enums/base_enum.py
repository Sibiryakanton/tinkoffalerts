import typing as t
from enum import Enum


class BaseEnum(Enum):
    @classmethod
    def choices(cls) -> t.Generator[t.Tuple[t.Any, t.Any], t.Any, None]:
        return ((x.name, x.value) for x in cls)
