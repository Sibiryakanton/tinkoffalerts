from django.utils.translation import gettext_lazy as _

from .base_enum import BaseEnum


class MarketProductTypes(BaseEnum):
    Stock = _("Stock")
    Bond = _("Bond")
    Etf = _("ETFS")
    Currency = _("Currency")
