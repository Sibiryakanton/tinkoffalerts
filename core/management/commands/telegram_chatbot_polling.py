import typing as t

from aiogram import executor, types
from django.conf import settings
from django.core.management.base import BaseCommand

from configuration.di import ProductionContainer


class Command(BaseCommand):
    help = "Start Telegram chatbot updates polling"

    def handle(self, *args: t.Any, **options: t.Any) -> None:
        dp = ProductionContainer.aiogram_dispatcher()
        if settings.TELEGRAM_BOT_KEY:
            executor.start_polling(
                dp, skip_updates=True, allowed_updates=types.AllowedUpdates.all()
            )
