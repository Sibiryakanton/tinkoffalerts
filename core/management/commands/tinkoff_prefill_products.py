import typing as t

from django.core.management.base import BaseCommand

from configuration.di import ProductionContainer
from core.models import MarketProduct


class Command(BaseCommand):
    """
    Temporary desicion, because we dont have any custom
    frontend interface for search products in
    tinkoff without saving on our side
    """

    help = "Fetch all Tinkoff products"

    def handle(self, *args: t.Any, **options: t.Any) -> None:
        manager = ProductionContainer.tinkoff_api_manager()
        methods: t.List[t.Callable[[], t.List[t.Any]]] = [
            manager.get_market_stocks,
            manager.get_market_bonds,
            manager.get_market_etfs,
            manager.get_market_currencies,
        ]
        for method in methods:
            products = method()
            formatted_data = [
                {
                    "title": info["name"],
                    "product_type": info["type"],
                    "figi": info["figi"],
                    "ticker": info["ticker"],
                }
                for info in products
            ]
            for data in formatted_data:
                figi = data.pop("figi")
                MarketProduct.objects.update_or_create(figi=figi, defaults=data)
