import typing as t

from django.core.management.base import BaseCommand

from configuration.di import ProductionContainer


class Command(BaseCommand):
    help = "Start websocket connection with Tinkoff.Investments for retrieving candles"

    def handle(self, *args: t.Any, **options: t.Any) -> None:
        connect = ProductionContainer.tinkoff_websocket()
        connect.run_forever()
