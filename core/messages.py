from django.utils.translation import gettext_lazy as _

MARKET_PRODUCT_WRONG_FIGI = _("The product with this FIGI number doesn't exists")
TELEGRAM_WRONG_EVENT_TYPE = "Telegram chatbot: wrong event type -"
TELEGRAM_USER_NOT_FOUND = "User not found"
TELEGRAM_NOTIFICATIONS_DISABLED = "The telegram notifications was disabled."

TELEGRAM_NOTIFICATIONS_ENABLED = "The telegram notifications was enabled."
