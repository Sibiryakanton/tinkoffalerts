import typing as t

from django.db import models
from django.utils.translation import gettext_lazy as _

from core.enums import SignalPriceTimeInterval


class MarketSignalQuerySet(models.QuerySet):
    def active(self) -> models.QuerySet["MarketSignal"]:
        return self.filter(is_active=True)


class MarketSignal(models.Model):
    user = models.ForeignKey(
        "accounts.User",
        on_delete=models.CASCADE,
        verbose_name=_("User"),
        related_name="user_signals",
    )
    product = models.ForeignKey(
        "MarketProduct",
        on_delete=models.CASCADE,
        verbose_name=_("Product"),
        related_name="product_signals",
    )

    price_percent_change = models.DecimalField(
        _("If the price changed on"), max_digits=3, decimal_places=1
    )

    time_interval = models.CharField(
        _("In this time interval"),
        max_length=50,
        choices=SignalPriceTimeInterval.choices(),
    )
    is_active = models.BooleanField(_("Waiting for triggering"), default=True)
    last_triggered_at = models.DateTimeField(
        _("Datetime of last signal's activating"), null=True, blank=True
    )
    objects = MarketSignalQuerySet.as_manager()

    def __init__(self, *args: t.Any, **kwargs: t.Any) -> None:
        super().__init__(*args, **kwargs)
        self._is_active = self.is_active

    def delete(self, *args: t.Any, **kwargs: t.Any) -> t.Any:
        collector_response: t.Any = super().delete(*args, **kwargs)
        if not self.product.product_signals.active().exists():
            self.product.is_need_candles = False
            self.product.save()
        return collector_response

    def save(self, *args: t.Any, **kwargs: t.Any) -> None:
        super().save(*args, **kwargs)
        if self.is_active and not self.product.is_need_candles:
            self.product.is_need_candles = True
            self.product.save()
        elif not self.product.product_signals.active().exists():
            self.product.is_need_candles = False
            self.product.save()

    @property
    def short_data(self) -> t.Dict[str, t.Any]:
        data = {
            "id": self.id,
            "product": self.product.short_data,
        }
        return data

    def __str__(self) -> str:
        return f"{self.product}|{self.price_percent_change}|{self.get_time_interval_display()}|{self.is_active}"

    class Meta:
        verbose_name = _("Market Signal")
        verbose_name_plural = _("Market Signals")
