import typing as t

from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext_lazy as _

from configuration.di import ProductionContainer
from core.enums import MarketProductTypes
from core.messages import MARKET_PRODUCT_WRONG_FIGI


class MarketProductQuerySet(models.QuerySet):
    def active(self) -> models.QuerySet:
        return self.filter(is_need_candles=True)

    def get_figi_for_subscription(
        self, current_subscriptions: t.List[str]
    ) -> models.QuerySet["MarketProduct"]:
        """
        Retrieve list of figi numbers for using in
         websocket connection for subscribing on new products.
        :param current_subscriptions: as written, it's a current
        subscriptions in websocket, which no need to return (list of strings).

        """
        return (
            self.active()
            .exclude(figi__in=current_subscriptions)
            .values_list("figi", flat=True)
        )

    def get_figi_for_unsubscription(
        self, current_subscriptions: t.List[str]
    ) -> models.QuerySet["MarketProduct"]:
        """
        Retrieve list of figi numbers for using in
         websocket connection for UNsubscribing from useless
          products (useless = there are no signals that
           would need candles from that product).
        :param current_subscriptions: current subscriptions in websocket (list of strings).
        """
        return self.filter(
            is_need_candles=False, figi__in=current_subscriptions
        ).values_list("figi", flat=True)


class MarketProduct(models.Model):
    """Model of Tinkoff Investments product - Stocks, Bonds etc."""

    title = models.CharField(_("Title"), max_length=200)
    ticker = models.CharField(max_length=20, verbose_name=_("Ticker"), blank=True)
    product_type = models.CharField(
        _("Product Type"), max_length=20, choices=MarketProductTypes.choices()
    )
    figi: str = models.CharField(_("FIGI number"), max_length=20)

    is_need_candles = models.BooleanField(_("Need Streaming Candles"), default=False)

    objects = MarketProductQuerySet.as_manager()

    def __init__(self, *args: t.Any, **kwargs: t.Any) -> None:
        super().__init__(*args, **kwargs)
        self._is_need_candles = self.is_need_candles

    def clean(self) -> None:
        """figi number validation (check if exists) and prefill other fields if needs)"""
        if not self.title or not self.product_type or not self.ticker:
            rest_manager = ProductionContainer.tinkoff_api_manager()
            check_figi = rest_manager.search_by_figi(self.figi)
            if check_figi.status_code == 200:
                info = check_figi.json()["payload"]
                self.title = info["name"]
                self.product_type = info["type"]
                self.ticker = info["ticker"]
            else:
                raise ValidationError({"figi": MARKET_PRODUCT_WRONG_FIGI})

    def save(self, *args: t.Any, **kwargs: t.Any) -> None:
        self.clean()
        super().save(*args, **kwargs)

    @property
    def short_data(self) -> t.Dict[str, t.Any]:
        data = {
            "id": self.id,
            "title": self.title,
            "product_type": self.product_type,
            "figi": self.figi,
        }
        return data

    def __str__(self) -> str:
        return f"{self.figi} | {self.title}"

    class Meta:
        verbose_name = _("Market Product")
        verbose_name_plural = _("Market Products")
