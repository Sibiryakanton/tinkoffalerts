import typing as t
from decimal import Decimal

from django.db import models
from django.utils.timezone import timedelta
from django.utils.translation import gettext_lazy as _

from core.enums import SignalPriceTimeIntervalIntegers
from core.services import TelegramNotificationGenerator, VKNotificationGenerator

if t.TYPE_CHECKING:
    from . import MarketProduct, MarketSignal


class CandleManager(models.Manager):
    def get_candles_for_signal(
        self,
        candle: "Candle",
        signal: "MarketSignal",
    ) -> t.Tuple[t.Optional["Candle"], t.Optional["Candle"]]:
        """
        Get candles queryset for calculate difference of price in current interval
        """
        interval = signal.time_interval
        interval_as_int = SignalPriceTimeIntervalIntegers.__getattr__(interval).value  # type: ignore

        range_end: timedelta = candle.start_time
        range_start = range_end - timedelta(minutes=interval_as_int)
        datetime_query = models.Q(start_time__range=[range_start, range_end])

        if signal.last_triggered_at:
            datetime_query &= models.Q(start_time__gt=signal.last_triggered_at)
        candles = (
            super()
            .get_queryset()
            .filter(datetime_query, product=candle.product)
            .order_by("start_time")
        )

        if not candles.count():
            return None, None

        # Find the lowest and highest price in the specified
        # interval .
        as_list = list(candles)
        last_candle = as_list[-1]

        if signal.price_percent_change > 0:
            first_candle = candles.order_by("open_price").first()
        else:
            first_candle = candles.order_by("-open_price").first()

        if first_candle.id == last_candle.id:
            return first_candle, None
        return first_candle, last_candle


class Candle(models.Model):
    product: "MarketProduct" = models.ForeignKey(
        "MarketProduct", on_delete=models.CASCADE, related_name="candles"
    )
    open_price: Decimal = models.DecimalField(
        _("Price on Opening"), max_digits=10, decimal_places=2
    )
    close_price: Decimal = models.DecimalField(
        _("Price on Closing"), max_digits=10, decimal_places=2
    )
    high_price: Decimal = models.DecimalField(
        _("Max price in candle"), max_digits=10, decimal_places=2
    )
    low_price: Decimal = models.DecimalField(
        _("Min price in candle"), max_digits=10, decimal_places=2
    )
    volume = models.PositiveIntegerField(_("Volume"))
    start_time = models.DateTimeField(_("Datetime of start the candle"))

    objects = CandleManager()

    def __str__(self) -> str:
        return (
            f"{self.product.title} (open: {self.open_price}, "
            f"close: {self.close_price}, "
            f"high: {self.high_price}, low: {self.low_price})"
        )

    def save(self, *args: t.Any, **kwargs: t.Any) -> None:
        super().save(*args, **kwargs)
        self.trigger_signals()

    def trigger_signals(self) -> None:
        """
        - Looking for active related signals;
        - Calc price difference for every signal individually (because
          we need to use attribute last_triggered_at which
          cuts the excess part of price history);
        - Generate VK notifications for triggered signals.
        """
        product_signals = self.product.product_signals.active()
        for signal in product_signals:
            first_candle, last_candle = Candle.objects.get_candles_for_signal(
                candle=self, signal=signal
            )
            print(first_candle, last_candle)
            if not first_candle or not last_candle:
                continue
            divided_price = last_candle.close_price / first_candle.open_price
            difference = (divided_price - 1) * 100
            positive_trigger = (
                difference > 0 and 0 < signal.price_percent_change < difference
            )
            negative_trigger = (
                difference < 0 and difference < signal.price_percent_change < 0
            )
            if positive_trigger or negative_trigger:
                VKNotificationGenerator.create_notification(
                    signal=signal,
                    first_candle=first_candle,
                    last_candle=last_candle,
                    difference=difference,
                )
                TelegramNotificationGenerator.create_notification(
                    signal=signal,
                    first_candle=first_candle,
                    last_candle=last_candle,
                    difference=difference,
                )
                signal.last_triggered_at = last_candle.start_time
                signal.save()

    @property
    def websocket_data(self) -> t.Dict[str, t.Any]:

        data = {
            "id": self.id,
            "product": self.product.short_data,
            "open_price": float(self.open_price),
            "close_price": float(self.close_price),
            "high_price": float(self.high_price),
            "low_price": float(self.low_price),
            "volume": self.volume,
            "start_time": str(self.start_time),
        }
        return data

    class Meta:
        verbose_name = _("Candle")
        verbose_name_plural = _("Candles")
