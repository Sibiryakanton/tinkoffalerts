import typing as t

import requests


class VKManager:
    """
    Logic with VK API integration:
    https://vk.com/dev/manuals

    """

    API_ROOT = "https://api.vk.com/method/"

    def __init__(self, access_token: str):
        super().__init__()
        self.params = {"v": 5.81, "access_token": access_token}

    def _make_url(self, rel_url: str) -> str:
        return "{}{}".format(self.API_ROOT, rel_url)

    def _request(
        self,
        method: t.Any,
        url: str,
        params: t.Optional[t.Dict[str, t.Any]] = None,
        **kwargs: t.Any
    ) -> requests.Response:
        """
        The common request template
        :param method: requests package function for make request
        :params url: absolute url for request (without query params)
        :params params: query params for adding to url
        """
        params = params or {}
        params.update(self.params)
        response: requests.Response = method(url=url, params=params, **kwargs)
        return response

    def send_message(self, user_id: int, message: str) -> t.Dict[str, t.Any]:
        """
        :param user_id: VK Profile id (only integer, alias not suits)
        :param message: text of message
        """
        url = self._make_url("messages.send")
        params = {"user_id": user_id, "message": message}
        response = self._request(requests.post, url=url, params=params)
        as_json: t.Dict[str, t.Any] = response.json()
        return as_json
