import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class SendEmailController:
    """
    By some reason Django "out of box" method
     for sending email just refused to work.
     This class is used as a forced measure.
    """

    def __init__(self, server: str, port: int, email: str, password: str) -> None:
        super().__init__()
        self.server = server
        self.port = port
        self.email = email
        self.password = password

    def send_email(self, receiver: str, subject: str, message: str) -> None:
        """
        Send email message
        """
        msg = MIMEMultipart()
        msg["From"] = self.email
        msg["To"] = receiver
        msg["Subject"] = subject
        msg.attach(MIMEText(message, "plain"))

        with smtplib.SMTP_SSL(self.server, self.port) as server:
            server.login(self.email, self.password)
            server.sendmail(self.email, receiver, msg.as_string())
