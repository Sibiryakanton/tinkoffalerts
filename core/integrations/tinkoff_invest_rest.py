import json
import typing as t

import requests


class TinkoffAPIRESTManager:
    """
    Logic with Tinkoff Investments REST API integration:
    https://tinkoffcreditsystems.github.io/invest-openapi/
    https://tinkoffcreditsystems.github.io/invest-openapi/swagger-ui/
    """

    API_ROOT = "https://api-invest.tinkoff.ru/openapi/sandbox/"

    def __init__(self, token: str):
        super().__init__()
        self.TOKEN = token

    @property
    def _headers(self) -> t.Dict[str, str]:
        return {
            "Authorization": "Bearer {}".format(self.TOKEN),
        }

    def _make_url(self, rel_url: str) -> str:
        return "{}{}".format(self.API_ROOT, rel_url)

    def _request(
        self,
        method: t.Any,
        url: str,
        params: t.Optional[t.Dict[str, t.Any]] = None,
        data: t.Optional[t.Dict[str, t.Any]] = None,
        headers: t.Optional[t.Dict[str, t.Any]] = None,
        **kwargs: t.Any
    ) -> requests.Response:
        """
        The common request template
        :param method: requests package function for make request
        :params url: absolute url for request (without query params)
        :params params: query params for adding to url
        :params data: body of request
        :params headers: headers of request, only auth token by default
        """
        if headers is None:
            headers = self._headers

        data_as_json_dump: t.Optional[str] = json.dumps(data) if data else None
        response: requests.Response = method(
            url=url, params=params, data=data_as_json_dump, headers=headers, **kwargs
        )
        return response

    def get_market_bonds(self) -> t.List[t.Any]:
        url = self._make_url("market/bonds")
        response = self._request(requests.get, url=url)
        stocks: t.List[t.Any] = response.json()["payload"]["instruments"]
        return stocks

    def get_market_etfs(self) -> t.List[t.Any]:
        url = self._make_url("market/etfs")
        response = self._request(requests.get, url=url)
        stocks: t.List[t.Any] = response.json()["payload"]["instruments"]
        return stocks

    def get_market_currencies(self) -> t.List[t.Any]:
        url = self._make_url("market/currencies")
        response = self._request(requests.get, url=url)
        stocks: t.List[t.Any] = response.json()["payload"]["instruments"]
        return stocks

    def get_market_stocks(self) -> t.List[t.Any]:
        url = self._make_url("market/stocks")
        response = self._request(requests.get, url=url)
        stocks: t.List[t.Any] = response.json()["payload"]["instruments"]
        return stocks

    def search_by_figi(self, figi: str) -> requests.Response:
        url = self._make_url("market/search/by-figi")
        params = {"figi": figi}
        response = self._request(requests.get, url=url, params=params)
        return response
