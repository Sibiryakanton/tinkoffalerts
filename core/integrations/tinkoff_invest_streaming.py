import json
import typing as t

import websocket
from django.apps import apps
from django.utils.timezone import datetime, make_aware


class TinkoffInvestStreaminWebsocket(websocket.WebSocketApp):
    """
    Integration with Tinkoff.Investment - Connect by websocket
    https://tinkoffcreditsystems.github.io/invest-openapi/marketdata/
    """

    url = "wss://api-invest.tinkoff.ru/openapi/md/v1/md-openapi/ws"
    subscriptions_figi: t.List[str] = []

    def __init__(self, token: str, *args: t.Any, **kwargs: t.Any) -> None:
        websocket.enableTrace(True)
        header = {
            "Authorization": "Bearer {}".format(token),
        }

        super().__init__(
            url=self.url,
            header=header,
            on_ping=self.on_ping,
            on_message=self.on_message,
            *args,
            **kwargs
        )

        self.MarketProduct = apps.get_model("core.MarketProduct")

    def on_message(self, message: str) -> None:
        """
        Create or update existing candle of product price

        :param message: string in json format. Example:
         {"payload":{"o":574.19,"c":574.19,"h":575,"l":574.17,"v":21,
        "time":"2021-05-18T04:10:00Z","interval":"1min","figi":"BBG000N9MNX3"},
        "event":"candle","time":"2021-05-18T04:10:50.08833365Z"}
        """
        from core.models.candle import Candle

        message_json: t.Dict[str, t.Any] = json.loads(message)

        event_type = message_json["event"]
        payload: t.Dict[str, t.Any] = message_json["payload"]
        if event_type == "candle":
            data = {
                "open_price": payload["o"],
                "close_price": payload["c"],
                "high_price": payload["h"],
                "low_price": payload["l"],
                "volume": payload["v"],
            }
            product = self.MarketProduct.objects.get(figi=payload["figi"])
            time_format = "%Y-%m-%dT%H:%M:%SZ"
            naive_start_time = datetime.strptime(payload["time"], time_format)
            aware_start_time = make_aware(naive_start_time)
            Candle.objects.update_or_create(
                product=product, start_time=aware_start_time, defaults=data
            )

    def on_ping(self, msg: t.Any) -> None:
        """
        Subscribe for new products and unsubscribe
         from products which no need to candles
        """

        included_products = self.MarketProduct.objects.get_figi_for_subscription(
            self.subscriptions_figi
        )
        excluded_products = self.MarketProduct.objects.get_figi_for_unsubscription(
            self.subscriptions_figi
        )
        for excluded in excluded_products:
            self.candle_unsubcribe(excluded)
        for included in included_products:
            self.candle_subcribe(included)

    def candle_subcribe(self, figi: str) -> t.Any:
        data = json.dumps(
            {"event": "candle:subscribe", "figi": figi, "interval": "1min"}
        )
        response = self.send(data)
        if figi not in self.subscriptions_figi:
            self.subscriptions_figi.append(figi)
        return response

    def candle_unsubcribe(self, figi: str) -> t.Any:
        data = json.dumps(
            {"event": "candle:subscribe", "figi": figi, "interval": "1min"}
        )
        response = self.send(data)
        if figi in self.subscriptions_figi:
            self.subscriptions_figi.remove(figi)
        return response
