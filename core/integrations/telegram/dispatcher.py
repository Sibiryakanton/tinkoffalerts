import typing as t

from aiogram import Dispatcher, types
from django.core.validators import ValidationError

from .backend_controller import get_user_profile, register_user, update_password
from .buttons import event_router
from .keyboards import MainMenuKeyboard, RegisterKeyboard

if t.TYPE_CHECKING:
    from accounts.models import User


async def check_is_auth(message: types.Message) -> t.Optional["User"]:
    message_as_dict = dict(message)

    user_id = message.from_user.id
    contact = message_as_dict.get("contact", None)

    register_keyboard = RegisterKeyboard()

    if contact:
        if contact["user_id"] == user_id:
            await register_user(
                user_id=user_id,
                first_name=contact["first_name"],
                last_name=contact["last_name"],
            )
            await message.answer("Отлично!", reply_markup=types.ReplyKeyboardRemove())

        else:
            await message.answer(
                "Передайте свои контакты",
                reply_markup=register_keyboard,
            )
            return None

    user_profile = await get_user_profile(telegram_id=user_id)
    if not user_profile:
        await message.answer(
            "Добро пожаловать! Зарегистрируйтесь, прежде чем использовать сервис: ",
            reply_markup=register_keyboard,
        )

    return user_profile


async def user_messages_handler(message: types.Message) -> None:
    user_profile = await check_is_auth(message)
    if user_profile:
        text = """
        Главное меню

        /change_password - сменить пароль от профиля
        внутри проекта Tinkoff Alerts (пароль нужен для входа в админку и создания
         сигналов). Пример: /change_password 1234
        """
        await message.answer(text, reply_markup=MainMenuKeyboard(user=user_profile))


async def change_password_handler(message: types.Message) -> None:
    user_profile = await check_is_auth(message)
    if user_profile:
        text = message["text"]
        password = text.replace("/change_password", "")
        try:
            user, _ = await update_password(user=user_profile, password=password)
            await message.answer("Пароль сменён.")
            await message.delete()

        except ValidationError as exc:
            msg = "\b".join(exc.messages)
            await message.answer(msg)
    await user_messages_handler(message)


async def query_handler(query: types.CallbackQuery) -> None:
    handler = event_router()
    await handler.process_event(query)


class TelegramChatBotDispatcher(Dispatcher):
    def __init__(self, *args: t.Any, **kwargs: t.Any) -> None:
        super().__init__(*args, **kwargs)
        self.register_message_handler(
            change_password_handler, commands=["change_password"]
        )
        self.register_message_handler(
            user_messages_handler, content_types=types.ContentType.all()
        )
        self.register_callback_query_handler(query_handler)
