from enum import Enum


class TelegramInlineButtonTypes(Enum):
    PROFILE = "profile"
    SWITCH_TELEGRAM = "switch_telegram_notifications"
