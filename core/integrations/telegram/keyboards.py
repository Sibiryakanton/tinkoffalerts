import typing as t

from aiogram import types

from .buttons import MyProfileButton, SwitchTelegramMessagesButton

if t.TYPE_CHECKING:
    from accounts.models import User


class BaseUserKeyboard(types.InlineKeyboardMarkup):
    def __init__(self, user: t.Optional["User"], *args: t.Any, **kwargs: t.Any) -> None:
        super().__init__(*args, **kwargs)
        self.user = user


class RegisterKeyboard(types.ReplyKeyboardMarkup):
    def __init__(self, *args: t.Any, **kwargs: t.Any) -> None:
        kwargs["resize_keyboard"] = True
        super().__init__(*args, **kwargs)
        register_button = {"text": "Регистрация", "request_contact": True}

        buttons_data = [register_button]
        buttons = [types.KeyboardButton(**data) for data in buttons_data]
        self.row(*buttons)


class MainMenuKeyboard(BaseUserKeyboard):
    buttons_classes = [MyProfileButton, SwitchTelegramMessagesButton]

    def __init__(self, *args: t.Any, **kwargs: t.Any) -> None:
        kwargs["resize_keyboard"] = True
        kwargs["one_time_keyboard"] = False

        super().__init__(*args, **kwargs)

        for button_class in self.buttons_classes:
            button = button_class(user=self.user)
            self.add(button)
