import random
import string
import typing as t

from asgiref.sync import sync_to_async
from django.contrib.auth import get_user_model
from django.contrib.auth.password_validation import validate_password

if t.TYPE_CHECKING:
    from accounts.models import User as UserModel


def user_model() -> "UserModel":
    User: "UserModel" = get_user_model()
    return User


async def get_user_profile(telegram_id: int) -> t.Optional["UserModel"]:
    profile: "UserModel" = await sync_to_async(
        user_model().objects.get_by_telegram_id, thread_sensitive=True
    )(telegram_id)
    return profile


async def register_user(
    user_id: int,
    first_name: t.Optional[str] = " ",
    last_name: t.Optional[str] = " ",
) -> t.Tuple["UserModel", str]:
    data = {
        "first_name": first_name,
        "last_name": last_name,
        "telegram_id": user_id,
        "username": user_id,
    }
    char_options = string.ascii_uppercase + string.digits
    password = "".join(random.choices(char_options, k=10))
    query = sync_to_async(user_model().objects.create_user)
    user = await query(**data)  # type: ignore
    await update_password(user=user, password=password)
    return user, password


async def update_password(
    user: "UserModel",
    password: str,
) -> t.Tuple["UserModel", str]:
    validate_password(password)
    user.set_password(password)
    await sync_to_async(user.save)()  # type: ignore
    return user, password
