import typing as t
from abc import abstractmethod

from aiogram import types
from asgiref.sync import sync_to_async

from core.messages import (
    TELEGRAM_NOTIFICATIONS_DISABLED,
    TELEGRAM_NOTIFICATIONS_ENABLED,
    TELEGRAM_USER_NOT_FOUND,
    TELEGRAM_WRONG_EVENT_TYPE,
)

from .backend_controller import get_user_profile
from .enums import TelegramInlineButtonTypes

if t.TYPE_CHECKING:
    from accounts.models import User


class BaseInlineButton(types.InlineKeyboardButton):
    @abstractmethod
    def handle_event(cls, query: types.CallbackQuery) -> t.Any:
        """
        The method for processing data retrieved from user query
        """
        raise NotImplementedError

    @property
    @abstractmethod
    def text_event(cls) -> str:
        """Text for button."""
        return cls.text_event

    def get_text_event(self) -> str:
        """
        the text for button
        """
        return self.text_event

    @property
    @abstractmethod
    def callback_data_event(cls) -> str:
        """
        the key for exact event type
        """
        return cls.callback_data_event

    def __init__(
        self, user: t.Optional["User"] = None, *args: t.Any, **kwargs: t.Any
    ) -> None:
        kwargs["callback_data"] = self.__class__.callback_data_event
        self.user = user
        kwargs["text"] = self.get_text_event()
        super().__init__(*args, **kwargs)


class MyProfileButton(BaseInlineButton):
    callback_data_event = TelegramInlineButtonTypes.PROFILE.value
    text_event = "Инфо о профиле"

    async def handle_event(self, query: types.CallbackQuery) -> None:
        user = await get_user_profile(query.from_user.id)
        if user:
            msg_data = {
                "Telegram ID": user.telegram_id,
                "Telegram messages enabled": bool(
                    user.is_telegram_enabled and user.telegram_id
                ),
                "VK": bool(user.is_vk_enabled and user.vk_id),
            }
            text_strings = [f"{key}: {value}" for key, value in msg_data.items()]
            text = "\n".join(text_strings)

            await query.answer(text=text, show_alert=True)
        else:
            await query.answer(text=TELEGRAM_USER_NOT_FOUND, show_alert=True)


class SwitchTelegramMessagesButton(BaseInlineButton):
    callback_data_event = TelegramInlineButtonTypes.SWITCH_TELEGRAM.value
    text_event = "Включить уведомления Telegram"

    def get_text_event(self) -> str:
        if self.user and self.user.is_telegram_enabled:
            self.text_event = "Выключить уведомления Telegram"
        return super().get_text_event()

    async def handle_event(self, query: types.CallbackQuery) -> None:
        from .keyboards import MainMenuKeyboard

        user = await get_user_profile(query.from_user.id)
        if user:
            user.is_telegram_enabled = not user.is_telegram_enabled
            await sync_to_async(user.save)()  # type: ignore
            text = (
                TELEGRAM_NOTIFICATIONS_ENABLED
                if user.is_telegram_enabled
                else TELEGRAM_NOTIFICATIONS_DISABLED
            )
            await query.answer(text=text, show_alert=True)
            await self.bot.send_message(
                chat_id=query.from_user.id,
                text="Главное меню",
                reply_markup=MainMenuKeyboard(user=user),
            )
            await self.bot.delete_message(
                chat_id=query.from_user.id, message_id=query.message.message_id
            )

        else:
            await query.answer(text=TELEGRAM_USER_NOT_FOUND, show_alert=True)


class InlineEventRouter:
    events: t.Dict[str, t.Type[BaseInlineButton]] = {}

    def add_button(self, button_class: t.Type[BaseInlineButton]) -> None:
        self.events[button_class.callback_data_event] = button_class  # type: ignore

    async def process_event(self, query: types.CallbackQuery) -> None:
        callback_data = query.data
        event_handler_class: t.Optional[t.Type[BaseInlineButton]] = self.events.get(
            callback_data, None
        )
        if event_handler_class:
            event_handler = event_handler_class()
            await event_handler.handle_event(query=query)
        else:
            msg = TELEGRAM_WRONG_EVENT_TYPE + callback_data
            raise Exception(msg)


def event_router() -> InlineEventRouter:
    event_router = InlineEventRouter()
    event_router.add_button(MyProfileButton)  # type: ignore
    event_router.add_button(SwitchTelegramMessagesButton)  # type: ignore

    return event_router
