import typing as t


def generate_notification_message(message_data: t.Dict[t.Any, t.Any]) -> str:
    title = message_data["title"]
    first_datetime = message_data["first"]["datetime"]
    first_open_price = message_data["first"]["open_price"]
    last_datetime = message_data["last"]["datetime"]
    last_close_price = message_data["last"]["close_price"]
    diff_rounded = message_data["diff"]

    message = f"""
    {title}
    {first_datetime} - {first_open_price}
    {last_datetime} - {last_close_price}
    {diff_rounded}%
    """
    return message
