from django.contrib import admin

from core.models import Candle


@admin.register(Candle)
class CandleAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "product",
        "start_time",
        "open_price",
        "close_price",
        "high_price",
        "low_price",
    ]
    list_display_links = ["id", "product", "start_time"]
    search_fields = ["product__id", "product__figi", "product__title"]
    raw_id_fields = ["product"]
