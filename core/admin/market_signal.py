import typing as t

from django.contrib import admin
from django.db.models import QuerySet
from django.utils.translation import gettext_lazy as _

from accounts.admin import UserFieldStrictForm
from core.models import MarketSignal


@admin.register(MarketSignal)
class MarketSignalAdmin(admin.ModelAdmin):
    form = UserFieldStrictForm

    list_display = [
        "id",
        "user",
        "product",
        "price_percent_change_symbol",
        "time_interval",
        "is_active",
    ]
    list_display_links = ["id", "user", "product", "time_interval"]
    list_filter = ["time_interval", "is_active"]
    list_editable = ["is_active"]
    raw_id_fields = ["product", "user"]
    actions = ["enable_signals"]

    def get_form(
        self, request: t.Any, obj: t.Optional[MarketSignal] = None, **kwargs: t.Any
    ) -> "MarketSignalAdmin":
        form: MarketSignalAdmin = super().get_form(request, obj, **kwargs)
        form.current_user = request.user
        return form

    def get_queryset(self, request: t.Any) -> QuerySet[MarketSignal]:
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(user=request.user)
        return queryset

    def price_percent_change_symbol(self, instance: MarketSignal) -> str:
        return f"{instance.price_percent_change}%"

    price_percent_change_symbol.short_description = _("Price change trigger")  # type: ignore

    def enable_signals(self, request: t.Any, queryset: QuerySet[MarketSignal]) -> None:
        queryset.update(is_active=True)

    enable_signals.short_description = _("Enable selected signals")  # type: ignore
