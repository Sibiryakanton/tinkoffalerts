from django.contrib import admin

from core.models import MarketProduct


@admin.register(MarketProduct)
class MarketProductAdmin(admin.ModelAdmin):
    list_display = ["id", "product_type", "figi", "ticker", "title", "is_need_candles"]
    list_display_links = ["id", "product_type", "figi", "title"]
    list_filter = ["product_type", "is_need_candles"]
    list_editable = ["is_need_candles"]
    readonly_fields = ["title", "product_type"]
    search_fields = ["figi", "title"]
