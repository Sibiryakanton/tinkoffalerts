import typing as t
from decimal import Decimal

from django.conf import settings

from accounts.models import VKNotification
from core.utils import generate_notification_message

from .tasks import send_telegram_msg

if t.TYPE_CHECKING:
    from accounts.models import User
    from core.models import Candle, MarketSignal


class BaseNotificationGenerator:
    is_enabled = False

    @classmethod
    def create_notification(
        cls,
        signal: "MarketSignal",
        first_candle: "Candle",
        last_candle: "Candle",
        difference: Decimal,
    ) -> None:
        if not cls.is_enabled:
            return
        diff_rounded = round(difference, 2)
        first_datetime = first_candle.start_time.strftime("%Y-%m-%d %H:%M:%S")
        last_datetime = last_candle.start_time.strftime("%Y-%m-%d %H:%M:%S")

        msg_data = {
            "title": signal.product.title,
            "first": {
                "datetime": first_datetime,
                "open_price": first_candle.open_price,
            },
            "last": {
                "datetime": last_datetime,
                "close_price": last_candle.close_price,
            },
            "diff": diff_rounded,
        }
        cls._handler(msg_data=msg_data, user=signal.user)

    @classmethod
    def _handler(cls, msg_data: t.Dict[t.Any, t.Any], user: "User") -> None:
        """
        Generate the notification with info about signal, candles and user
        :param msg_data: Info about market, first candle and last candle
        :param user: User instance of retriever
        """
        raise NotImplementedError


class VKNotificationGenerator(BaseNotificationGenerator):
    is_enabled = bool(settings.VK_SERVICE_VK_GROUP_KEY)

    @classmethod
    def _handler(cls, msg_data: t.Dict[t.Any, t.Any], user: "User") -> None:
        """
        Generate the VK notification with info about
        changing price related to signal
        :param msg_data: Info about market, first candle and last candle
        :param user: User instance of retriever
        """

        message_text = generate_notification_message(msg_data)
        VKNotification.objects.create(user=user, message=message_text)


class TelegramNotificationGenerator(BaseNotificationGenerator):
    is_enabled = bool(settings.TELEGRAM_BOT_KEY)

    @classmethod
    def _handler(cls, msg_data: t.Dict[t.Any, t.Any], user: "User") -> None:
        """
        Generate the VK notification with info about
        changing price related to signal
        :param msg_data: Info about market, first candle and last candle
        :param user: User instance of retriever
        """
        telegram_id = user.telegram_id
        if user.is_telegram_enabled and telegram_id:
            data = {
                "msg": msg_data,
                "user_id": telegram_id,
            }
            send_telegram_msg.delay(data)
