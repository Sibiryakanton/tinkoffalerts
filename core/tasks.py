import asyncio
import typing

from celery import shared_task
from django.utils.timezone import now, timedelta

from configuration.di import ProductionContainer
from core.utils import generate_notification_message


@shared_task
def clear_excess_candles() -> None:
    """
    We don't need candles older than 7 days
    """
    from core.models import Candle

    Candle.objects.filter(start_time__lte=now() - timedelta(days=7)).delete()


@shared_task
def send_telegram_msg(msg_data: typing.Dict[typing.Any, typing.Any]) -> None:
    telegram_bot = ProductionContainer.aiogram_bot()

    msg = msg_data["msg"]
    message_text = generate_notification_message(msg)

    asyncio.run(
        telegram_bot.send_message(chat_id=msg_data["user_id"], text=message_text)
    )
