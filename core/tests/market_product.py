import typing as t
from unittest.mock import patch

from django.core.exceptions import ValidationError
from django.core.management import call_command

from core.base_test import BaseDjangoTestCase, MockHTTPResponse
from core.enums import SignalPriceTimeInterval
from core.models import MarketProduct, MarketSignal


class MarketProductTestCase(BaseDjangoTestCase):
    @patch("requests.post")
    def test_autofill(self, request_post_mock: t.Any) -> None:
        data = {
            "trackingId": "432d4b6517b7e0af",
            "payload": {
                "figi": "BBG000B9XRY4",
                "ticker": "AAPL",
                "isin": "US0378331005",
                "minPriceIncrement": 0.01,
                "lot": 1,
                "currency": "USD",
                "name": "Apple",
                "type": "Stock",
            },
            "status": "Ok",
        }
        response = MockHTTPResponse(status_code=201, json=data)
        request_post_mock.return_value = response

        product = MarketProduct(figi="BBG000B9XRY4")
        product.save()
        self.assertEqual(product.title, "Apple")

    @patch("requests.post")
    def test_autofill_fail(self, request_post_mock: t.Any) -> None:
        response = MockHTTPResponse(status_code=400, json={"some": "data"})
        request_post_mock.return_value = response

        product = MarketProduct(figi="some_not_exists_figi")
        self.assertRaises(ValidationError, product.save)

    def test_get_figi_for_subscription(self) -> None:
        current_subscriptions = [self.unsubscribed_product.figi]
        queryset = MarketProduct.objects.get_figi_for_subscription(
            current_subscriptions
        )
        self.assertTrue(queryset.exists())

    def test_get_figi_for_unsubscription(self) -> None:
        current_subscriptions = [self.unsubscribed_product.figi]
        queryset = MarketProduct.objects.get_figi_for_unsubscription(
            current_subscriptions
        )
        self.assertTrue(queryset.exists())

    def test_product_subscribe_change_delete_signal(self) -> None:
        self.subscribed_product.product_signals.all().delete()
        self.signal = MarketSignal.objects.create(
            user=self.quest,
            product=self.subscribed_product,
            price_percent_change=1,
            is_active=True,
            time_interval=SignalPriceTimeInterval.min_30.name,
        )
        self.assertTrue(self.subscribed_product.is_need_candles)
        self.signal.delete()
        self.assertFalse(self.subscribed_product.is_need_candles)

    def test_product_subscribe_change_disable_signal(self) -> None:
        self.subscribed_product.product_signals.all().delete()
        self.signal = MarketSignal.objects.create(
            user=self.quest,
            product=self.subscribed_product,
            price_percent_change=1,
            is_active=True,
            time_interval=SignalPriceTimeInterval.min_30.name,
        )
        self.assertTrue(self.subscribed_product.is_need_candles)
        self.signal.is_active = False
        self.signal.save()
        self.assertFalse(self.subscribed_product.is_need_candles)

    @patch("requests.get")
    def test_prefill_products(self, get_mock: t.Any) -> None:
        data = {
            "payload": {
                "instruments": [
                    {
                        "figi": "BBG333333333",
                        "ticker": "TMOS",
                        "isin": "RU000A101X76",
                        "minPriceIncrement": 0.002,
                        "lot": 1,
                        "currency": "RUB",
                        "name": "Тинькофф iMOEX",
                        "type": "Etf",
                    }
                ]
            }
        }
        response = MockHTTPResponse(status_code=200, json=data)
        get_mock.return_value = response
        call_command("tinkoff_prefill_products")
