import typing as t
from unittest.mock import patch

from configuration.di import ProductionContainer
from core.base_test import BaseDjangoTestCase, MockHTTPResponse


class TinkoffInvestRESTTestCase(BaseDjangoTestCase):
    manager = ProductionContainer.tinkoff_api_manager()

    @patch("requests.get")
    def test_get_market_stocks(self, get_mock: t.Any) -> None:
        data = {
            "payload": {
                "instruments": [
                    {
                        "figi": "BBG000HLJ7M4",
                        "ticker": "IDCC",
                        "isin": "US45867G1013",
                        "minPriceIncrement": 0.01,
                        "lot": 1,
                        "currency": "USD",
                        "name": "InterDigItal Inc",
                        "type": "Stock",
                    }
                ]
            }
        }
        response = MockHTTPResponse(status_code=200, json=data)
        get_mock.return_value = response
        self.manager.get_market_stocks()

    @patch("requests.get")
    def test_get_market_bounds(self, get_mock: t.Any) -> None:
        data = {
            "payload": {
                "instruments": [
                    {
                        "figi": "BBG00T22WKV5",
                        "ticker": "SU29013RMFS8",
                        "isin": "RU000A101KT1",
                        "minPriceIncrement": 0.01,
                        "faceValue": 1000.0,
                        "lot": 1,
                        "currency": "RUB",
                        "name": "ОФЗ 29013",
                        "type": "Bond",
                    }
                ]
            }
        }
        response = MockHTTPResponse(status_code=200, json=data)
        get_mock.return_value = response
        self.manager.get_market_bonds()

    @patch("requests.get")
    def test_get_market_etfs(self, get_mock: t.Any) -> None:
        data = {
            "payload": {
                "instruments": [
                    {
                        "figi": "BBG333333333",
                        "ticker": "TMOS",
                        "isin": "RU000A101X76",
                        "minPriceIncrement": 0.002,
                        "lot": 1,
                        "currency": "RUB",
                        "name": "Тинькофф iMOEX",
                        "type": "Etf",
                    }
                ]
            }
        }
        response = MockHTTPResponse(status_code=200, json=data)
        get_mock.return_value = response
        self.manager.get_market_etfs()

    @patch("requests.get")
    def test_get_market_currencies(self, get_mock: t.Any) -> None:
        data = {
            "payload": {
                "instruments": [
                    {
                        "figi": "BBG0013HGFT4",
                        "ticker": "USD000UTSTOM",
                        "minPriceIncrement": 0.0025,
                        "lot": 1000,
                        "currency": "RUB",
                        "name": "Доллар США",
                        "type": "Currency",
                    }
                ]
            }
        }
        response = MockHTTPResponse(status_code=200, json=data)
        get_mock.return_value = response
        self.manager.get_market_currencies()
