from core.admin import MarketSignalAdmin
from core.base_test import BaseDjangoTestCase
from core.models import MarketSignal


class CoreAdminTestCase(BaseDjangoTestCase):
    def test_market_signal_get_form(self) -> None:
        model_admin = MarketSignalAdmin(model=MarketSignal, admin_site=self.site)
        model_admin.get_form(
            request=self.request_mock(self.admin_user), obj=self.signal
        )

    def test_market_signal_get_queryset_admin(self) -> None:
        model_admin = MarketSignalAdmin(model=MarketSignal, admin_site=self.site)
        request = self.request_mock(self.admin_user)
        notification_retrieved = model_admin.get_queryset(request=request).exists()
        self.assertTrue(notification_retrieved)

    def test_market_signal_get_queryset_quest(self) -> None:
        self.signal.user = self.admin_user
        self.signal.save()
        model_admin = MarketSignalAdmin(model=MarketSignal, admin_site=self.site)
        request = self.request_mock(self.quest)
        notification_retrieved = model_admin.get_queryset(request=request).exists()
        self.assertFalse(notification_retrieved)

    def test_market_signal_price_percent_change_symbol(self) -> None:
        model_admin = MarketSignalAdmin(model=MarketSignal, admin_site=self.site)
        model_admin.price_percent_change_symbol(instance=self.signal)

    def test_market_signal_enable_signals(self) -> None:
        model_admin = MarketSignalAdmin(model=MarketSignal, admin_site=self.site)
        request = self.request_mock(self.quest)
        model_admin.enable_signals(
            request=request, queryset=self.quest.user_signals.all()
        )
