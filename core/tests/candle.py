from django.utils.timezone import now, timedelta

from core.base_test import BaseDjangoTestCase
from core.enums import SignalPriceTimeInterval
from core.models import Candle, MarketSignal


class CandlesTestCase(BaseDjangoTestCase):
    def prep_candles(self) -> None:

        self.admin_user.user_signals.all().delete()
        Candle.objects.create(
            product=self.subscribed_product,
            open_price=300,
            close_price=200,
            low_price=200,
            high_price=300,
            volume=2,
            start_time=now() - timedelta(minutes=7),
        )
        self.second_test_min_candle = Candle.objects.create(
            product=self.subscribed_product,
            open_price=500,
            close_price=400,
            low_price=200,
            high_price=400,
            start_time=now() - timedelta(minutes=6),
            volume=2,
        )
        Candle.objects.create(
            product=self.subscribed_product,
            open_price=400,
            close_price=300,
            low_price=300,
            high_price=400,
            start_time=now() - timedelta(minutes=5),
            volume=2,
        )
        Candle.objects.create(
            product=self.subscribed_product,
            open_price=315,
            close_price=400,
            low_price=300,
            high_price=400,
            start_time=now() - timedelta(minutes=4),
            volume=2,
        )
        self.max_candle = Candle.objects.create(
            product=self.subscribed_product,
            open_price=400,
            close_price=600,
            low_price=400,
            high_price=600,
            start_time=now() - timedelta(minutes=3),
            volume=2,
        )
        self.first_test_min_candle = Candle.objects.create(
            product=self.subscribed_product,
            open_price=100,
            close_price=300,
            low_price=100,
            high_price=300,
            start_time=now() - timedelta(minutes=2),
            volume=2,
        )
        self.last_candle = Candle.objects.create(
            product=self.subscribed_product,
            open_price=400,
            close_price=100,
            low_price=400,
            high_price=300,
            start_time=now() - timedelta(minutes=1),
            volume=2,
        )

    def test_min_max_candle(self) -> None:
        self.prep_candles()
        self.signal = MarketSignal.objects.create(
            user=self.quest,
            product=self.subscribed_product,
            price_percent_change=2,
            is_active=True,
            time_interval=SignalPriceTimeInterval.min_30.name,
        )
        first_cendle, second_candle = Candle.objects.get_candles_for_signal(
            candle=self.last_candle, signal=self.signal
        )
        assert first_cendle and second_candle
        self.assertEqual(first_cendle.id, self.first_test_min_candle.id)
        self.assertEqual(second_candle.id, self.last_candle.id)
