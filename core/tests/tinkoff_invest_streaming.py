import json
import typing as t
from unittest.mock import patch

from configuration.di import ProductionContainer
from core.base_test import BaseDjangoTestCase


class TinkoffInvestStreaminWebsocketTestCase(BaseDjangoTestCase):
    websocket_client = ProductionContainer.tinkoff_websocket()

    def test_on_message(self) -> None:
        close_price = 600.00
        data = {
            "payload": {
                "o": close_price,
                "c": close_price,
                "h": 620,
                "l": 574.17,
                "v": 21,
                "time": "2021-05-18T04:10:00Z",
                "interval": "1min",
                "figi": self.subscribed_product.figi,
            },
            "event": "candle",
            "time": "2021-05-18T04:10:50.08833365Z",
        }
        msg = json.dumps(data)

        self.websocket_client.on_message(msg)
        self.assertEqual(self.subscribed_product.candles.count(), 1)
        candle = self.subscribed_product.candles.last()
        self.assertEqual(float(candle.close_price), close_price)

    def test_on_message_repeat_tick(self) -> None:
        close_price = 600.00
        data = {
            "payload": {
                "o": close_price,
                "c": close_price,
                "h": 620.00,
                "l": 574.17,
                "v": 21,
                "time": "2021-05-18T04:10:00Z",
                "interval": "1min",
                "figi": self.subscribed_product.figi,
            },
            "event": "candle",
            "time": "2021-05-18T04:10:50.08833365Z",
        }
        msg = json.dumps(data)
        self.websocket_client.on_message(msg)
        self.assertEqual(self.subscribed_product.candles.count(), 1)
        candle = self.subscribed_product.candles.last()
        self.assertEqual(float(candle.close_price), close_price)

    def test_on_message_second_candle(self) -> None:
        self.assertIsNone(self.signal.last_triggered_at)
        self.test_on_message()

        close_price = 700.00
        data = {
            "payload": {
                "o": 600.00,
                "c": close_price,
                "h": 620.00,
                "l": 574.17,
                "v": 21,
                "time": "2021-05-18T04:11:00Z",
                "interval": "1min",
                "figi": self.subscribed_product.figi,
            },
            "event": "candle",
            "time": "2021-05-18T04:10:51.08833365Z",
        }
        msg = json.dumps(data)
        self.websocket_client.on_message(msg)

        self.assertEqual(self.subscribed_product.candles.count(), 2)
        candle = self.subscribed_product.candles.last()
        self.assertEqual(float(candle.close_price), close_price)
        self.signal.refresh_from_db()
        self.assertIsNotNone(self.signal.last_triggered_at)
        self.signal.last_triggered_at = None
        self.signal.save()
        self.websocket_client.on_message(msg)
        self.websocket_client.on_message(msg)

    @patch("core.integrations.TinkoffInvestStreaminWebsocket.send")
    def test_on_ping(self, send_mock: t.Any) -> None:
        send_mock.return_value = None

        self.unsubscribed_product.is_need_candles = False
        self.unsubscribed_product.save()

        self.websocket_client.subscriptions_figi = [self.unsubscribed_product.figi]
        self.websocket_client.on_ping("")
