import typing as t

from django.conf import settings
from django.contrib.admin.sites import AdminSite
from django.test import TestCase

from accounts.models import User
from core.enums import MarketProductTypes, SignalPriceTimeInterval
from core.models import MarketProduct, MarketSignal


class MockRequest:
    user: t.Optional[User] = None


class MockHTTPResponse:
    """
    Object for mocking "requests" package methods
    """

    def __init__(
        self,
        json: t.Dict[t.Any, t.Any],
        status_code: int,
        *args: t.Any,
        **kwargs: t.Any
    ):
        super().__init__()
        self.status_code = status_code
        self.json_data = json

    def json(self) -> t.Dict[t.Any, t.Any]:
        return self.json_data


class BaseDjangoTestCase(TestCase):
    first_password = "password123!"

    def setUp(self) -> None:
        super().setUp()
        settings.ROLLBAR = {"enabled": False}
        settings.DEFAULT_FILE_STORAGE = "django.core.files.storage.FileSystemStorage"

        self.site = AdminSite()

        self.admin_user = User.objects.create(
            username="admin",
            email="admin@mail.ru",
            vk_id="some_vk_id",
            is_active=True,
            is_staff=True,
            is_superuser=True,
        )
        self.admin_user.set_password(self.first_password)
        self.admin_user.save()

        self.quest = User.objects.create(
            username="quest",
            email="quest@mail.ru",
            vk_id="some_quest_vk_id",
            is_active=True,
            is_staff=True,
            is_superuser=False,
        )
        self.quest.set_password(self.first_password)
        self.quest.save()

        self.subscribed_product = MarketProduct.objects.create(
            figi="BBG000N9MNX3",
            ticker="123",
            product_type=MarketProductTypes.Stock.name,
            title="Tesla Motors",
            is_need_candles=True,
        )
        self.unsubscribed_product = MarketProduct.objects.create(
            figi="BBG000BNGBW9",
            title="Nokia",
            is_need_candles=False,
            ticker="1234",
            product_type=MarketProductTypes.Stock.name,
        )
        self.signal = MarketSignal.objects.create(
            user=self.quest,
            product=self.subscribed_product,
            price_percent_change=1,
            is_active=True,
            time_interval=SignalPriceTimeInterval.min_30.name,
        )

    def request_mock(self, user: t.Optional["User"] = None) -> MockRequest:
        """
        retrieve mock if Request object for testing views (for admin panels)
        """
        request = MockRequest()
        request.user = user or self.admin_user
        return request
