from python:3.9.6-buster

RUN python --version

WORKDIR /app
COPY ./requirements.txt ./requirements.txt
RUN pip install --upgrade -r requirements.txt

COPY . .
