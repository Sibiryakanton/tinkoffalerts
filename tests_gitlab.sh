#!/bin/bash
set -e
coverage run manage.py test
coverage report -m
coverage xml > coverage.xml
coverage-badge
