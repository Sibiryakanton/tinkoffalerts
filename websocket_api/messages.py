from django.utils.translation import gettext_lazy as _

AUTH_REQUIRED = _("Only authorized users required.")
