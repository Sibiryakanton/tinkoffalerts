from django.apps import AppConfig


class WebsocketNotifyConfig(AppConfig):
    name = "websocket_api"
