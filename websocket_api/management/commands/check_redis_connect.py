import typing as t

import channels.layers
from asgiref.sync import async_to_sync
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def handle(self, *args: t.Any, **options: t.Any) -> None:
        channel_layer = channels.layers.get_channel_layer()
        async_to_sync(channel_layer.send)(  # type: ignore
            "test_channel", {"channels layers OK": "True"}
        )
        async_to_sync(channel_layer.receive)("test_channel")  # type: ignore
