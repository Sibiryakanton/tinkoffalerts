import json
import typing as t

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from channels.layers import get_channel_layer

from websocket_api.messages import AUTH_REQUIRED


class NotificationsConsumer(WebsocketConsumer):
    """
    Consumer for sending notifications with change
     product price (create/update candle) and triggering signals
    """

    def connect(self) -> None:
        self.user = self.scope.get("user")
        if not self.user:
            self.room_group_name = "user_anon"
            self.close({"error": str(AUTH_REQUIRED)})
            return

        self.room_group_name = "user_%s" % self.user.id

        async_to_sync(self.channel_layer.group_add)(  # type: ignore
            self.room_group_name, self.channel_name
        )
        self.accept()

        data = {"status": "OK"}
        self.send(text_data=json.dumps({"event_type": "test_message", "message": data}))

    def disconnect(self, code: t.Any) -> None:
        async_to_sync(self.channel_layer.group_discard)(  # type: ignore
            self.room_group_name, self.channel_name
        )
        print("DISCONNECTED CODE: ", code)

    def receive(self, text_data: t.Any) -> None:
        if text_data == "ping":
            self.send(text_data=json.dumps({"event_type": "pong", "message": "pong"}))

    def test_message(self, event: t.Dict[str, t.Any]) -> None:
        message = event["message"]
        data = {"type": "new_message", "data": message}
        self.send(text_data=json.dumps({"message": data}))

    def product_candle(self, event: t.Dict[str, t.Any]) -> None:
        message = event["message"]
        data = {"type": "product_candle", "data": message}
        self.send(text_data=json.dumps({"message": data}))

    def signal_trigger(self, event: t.Dict[str, t.Any]) -> None:
        message = event["message"]
        data = {"type": "signal_trigger", "data": message}
        self.send(text_data=json.dumps({"message": data}))


def product_candle_update(user_id: int, candle_data: t.Dict[str, t.Any]) -> None:
    """Send the user a notification about the change in the price of an product."""
    group_name = "user_%s" % user_id
    data = {"type": "product_candle", "message": candle_data}
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(group_name, data)  # type: ignore


def signal_trigger(user_id: int, signal_data: t.Dict[str, t.Any]) -> None:
    """Send the user a notification about the triggering product signal."""
    group_name = "user_%s" % user_id
    data = {"type": "signal_trigger", "message": signal_data}
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(group_name, data)  # type: ignore
