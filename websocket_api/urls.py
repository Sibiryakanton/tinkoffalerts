from channels.routing import URLRouter
from django.conf.urls import url

from . import consumers

router = URLRouter(
    [
        url(r"^ws/notifications/$", consumers.NotificationsConsumer.as_asgi()),
    ]
)
