import json

from channels.testing import WebsocketCommunicator

from rest.tests.base import BaseTestCase

from .consumers import NotificationsConsumer


class NotificationsTestCase(BaseTestCase):
    async def test_notifications_auth(self) -> None:
        communicator = WebsocketCommunicator(
            NotificationsConsumer.as_asgi(), "/ws/notifications/"
        )
        communicator.scope["user"] = self.admin_user
        connected, subprotocol = await communicator.connect()
        assert connected
        response = await communicator.receive_from()
        self.assertEqual(
            response,
            json.dumps({"event_type": "test_message", "message": {"status": "OK"}}),
        )
        await communicator.disconnect()
