import traceback
import typing as t

import jwt
from channels.db import database_sync_to_async
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser

if t.TYPE_CHECKING:
    from accounts.models import User as UserType

User: "UserType" = get_user_model()


@database_sync_to_async
def get_user(user_id: int) -> t.Union["UserType", AnonymousUser]:
    try:
        return User.objects.get(id=user_id)
    except User.DoesNotExist:
        return AnonymousUser()


class CustomTokenAuthMiddlewareInstance:
    """
    Token Authorization middleware instance for Django Channels
    """

    def __init__(self, scope: t.Any, middleware: t.Any) -> None:
        self.middleware = middleware
        self.scope = dict(scope)
        self.inner = self.middleware.inner

    async def __call__(self, receive: t.Any, send: t.Any) -> t.Any:
        scope = self.scope
        headers = dict(scope["headers"])
        if b"authorization" in headers:
            token_name, token_key = headers[b"authorization"].decode().split()
            try:
                user_jwt = jwt.decode(
                    token_key, settings.SECRET_KEY, algorithms="HS256"
                )
                user = await get_user(user_jwt["user_id"])
                scope["user"] = user
            except Exception:
                traceback.print_exc()
        return await self.inner(scope, receive, send)


class CustomTokenAuthMiddleware:
    """Custom authentication of a user using a token obtained from the REST API."""

    def __init__(self, inner: t.Any) -> None:
        self.inner = inner

    def __call__(self, scope: t.Any) -> "CustomTokenAuthMiddlewareInstance":
        return CustomTokenAuthMiddlewareInstance(scope, self)
